@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/pratos') }}">Prato</a> :
@endsection
@section("contentheader_description", $prato->$view_col)
@section("section", "Pratos")
@section("section_url", url(config('laraadmin.adminRoute') . '/pratos'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Pratos Edit : ".$prato->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($prato, ['route' => [config('laraadmin.adminRoute') . '.pratos.update', $prato->id ], 'method'=>'PUT', 'id' => 'prato-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'nome')
					@la_input($module, 'imagem')
					@la_input($module, 'ingredientes')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/pratos') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#prato-edit-form").validate({
		
	});
});
</script>
@endpush
