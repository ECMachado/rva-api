# RVA API

Para já vamos fazer de uma maneira mais fácil. Em vez de instalares e configurares, guardei-te backup da base de dados.
Portanto, terminal na raiz do projecto e:

#### 1 - Inicializar container para correr a app
`lando start`

#### 2 - Instalar dependências PHP
`lando composer install`

#### 3 - Importar BD
`lando db-import `

#### 4 - Usar

Backoffice -  http://rva.lndo.site

API - http://rva.lndo.site/api/v1

API - http://rva.lndo.site/api/v1/pratos

API - http://rva.lndo.site/api/v1/pratos/{id}

API - http://rva.lndo.site/api/v1/ingredientes

API - http://rva.lndo.site/api/v1/ingredientes/{id}


PHPMyAdmin - http://pma.rva.lndo.site

(base de dados: laravel)
