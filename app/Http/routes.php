<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'api/v1'], function () {

  Route::get('/', function () {
    try {
      $pratos = \App\Models\Prato::get();

      foreach ($pratos as $prato) {
        $ingredientes =  (array) json_decode($prato->ingredientes);

        foreach ($ingredientes as $key => $ingrediente) {
          $ingredientes[$key] = \App\Models\Ingrediente::findOrFail($ingrediente);
        }
        $prato->ingredientes = $ingredientes;
      }
    }
    catch (\Exception $e) {
      return ['error' => $e];
    }

    return [
      'name' => 'RVA',
      'version' => '0.2.0',
      'url' => \Illuminate\Support\Facades\URL::current(),
      'app' => [
        'pratos' => $pratos,
      ]
    ];
  });

  Route::resource('/pratos', 'PratosController');
  Route::resource('/ingredientes', 'IngredientesController');
});

Route::get('/', function () {
    return redirect('/backstage');
});

/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';
