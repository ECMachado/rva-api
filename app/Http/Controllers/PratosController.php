<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Prato;
use App\Models\Ingrediente;

class PratosController extends Controller
{
  /**
	 * Display a listing of the Pratos.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
    try {
      $pratos = Prato::get();

      foreach ($pratos as $prato) {
        $ingredientes =  (array) json_decode($prato->ingredientes);

        foreach ($ingredientes as $key => $ingrediente) {
          $ingredientes[$key] = Ingrediente::findOrFail($ingrediente);
        }
        $prato->ingredientes = $ingredientes;
      }

      return [ "Pratos" => $pratos];
    }
    catch (\Exception $e) {
      return ['error' => $e];
    }
  }

  /**
	 * Display the specified prato.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
    $prato = Prato::findOrFail($id);

    if (sizeof($prato) > 0) {
			$ingredientes =  (array) json_decode($prato->ingredientes);

			foreach ($ingredientes as $key => $ingrediente) {
				$ingredientes[$key] = Ingrediente::findOrFail($ingrediente);
			}
			$prato->ingredientes = $ingredientes;

      return $prato;
    }
    else {
      return ['error' => 'Not found'];
    }
	}
}
