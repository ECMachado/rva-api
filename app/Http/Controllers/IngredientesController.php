<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Ingrediente;

class IngredientesController extends Controller
{
  /**
	 * Display a listing of the Pratos.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
    try {
      $ingredientes = Ingrediente::get();

      return $ingredientes;
    }
    catch (\Exception $e) {
      return ['error' => $e];
    }
  }

  /**
	 * Display the specified prato.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
    $ingrediente = Ingrediente::find($id);

    if (sizeof($ingrediente) > 0) {
      return $ingrediente;
    }
    else {
      return ['error' => 'Not found'];
    }
	}
}
